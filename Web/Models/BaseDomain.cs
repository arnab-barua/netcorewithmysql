﻿using System;

namespace Web.Models
{
    public class BaseDomain
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
