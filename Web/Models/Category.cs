﻿
namespace Web.Models
{
    public class Category : BaseDomain
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
    }
}
